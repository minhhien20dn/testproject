package com.example.myProject.controller;

import com.example.myProject.model.hoatDong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.example.myProject.service.IHoatDongService;



import java.util.List;

@Controller
public class hdController {
    @Autowired
    private IHoatDongService hoatDongService;

    // display list of employees
    @GetMapping("/")
    public String viewAll(Model model) {
        List<hoatDong> hoatDongList = hoatDongService.getAll();
        model.addAttribute("hoatDongList", hoatDongList);
        return "index";
    }

    @GetMapping("/themHoatDongMoi")
    public String themHoatDong(Model model) {
        hoatDong hd = new hoatDong();
        model.addAttribute("hd", hd);
        return "add";
    }

//    @PostMapping("/luuHoatDong")
    @RequestMapping(value = "/luuHoatDong", method = RequestMethod.POST)
    public String saveHoatDong(@ModelAttribute("hd") hoatDong hd) {
        hoatDongService.saveHoatDong(hd);
        return "redirect:/";
    }

    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable ( value = "id") int id, Model model) {

        hoatDong hd = hoatDongService.findById(id);

        model.addAttribute("hd", hd);
        return "edit";
    }

    @GetMapping("/deleteHoatDong/{id}")
    public String deleteHoatDong(@PathVariable (value = "id") int id) {
        this.hoatDongService.deleteHoatDongById(id);
        return "redirect:/";
    }

    

}
