package com.example.myProject.responsitory;

import com.example.myProject.model.hoatDong;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface hoatDongRes extends JpaRepository<hoatDong, Integer> {
}
