package com.example.myProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyProjectApplication {
//	@Autowired
//	private JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(MyProjectApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		String sql = "SELECT * FROM hoatdong";
//		List<hoatDong> hoatDongs = jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(hoatDong.class));
//		hoatDongs.forEach(System.out ::println);
//	}
}
