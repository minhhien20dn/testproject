package com.example.myProject.service;

import com.example.myProject.model.hoatDong;

import java.util.List;

public interface IHoatDongService {

    List<hoatDong> getAll();
    void saveHoatDong(hoatDong hd);
    hoatDong findById(int id);
    void deleteHoatDongById(int id);

    public hoatDong suaHoatDong(int maHD, hoatDong hoatDong);

}
