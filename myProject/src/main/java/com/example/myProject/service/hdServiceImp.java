package com.example.myProject.service;

import com.example.myProject.model.hoatDong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.myProject.responsitory.hoatDongRes;

import java.util.List;
import java.util.Optional;

@Service
public class hdServiceImp implements IHoatDongService{
    @Autowired
    private hoatDongRes hoatdongRes;

    @Override
    public List<hoatDong> getAll() {
        return hoatdongRes.findAll();
    }

    @Override
    public void saveHoatDong(hoatDong hd) {
        this.hoatdongRes.save(hd);
    }

    @Override
    public hoatDong findById(int id) {
        Optional<hoatDong> optional = hoatdongRes.findById(id);
        hoatDong hd = null;
        if (optional.isPresent()) {
            hd = optional.get();
        } else {
            throw new RuntimeException(" Not found for id :: " + id);
        }
        return hd;
    }

    @Override
    public void deleteHoatDongById(int id) {
        this.hoatdongRes.deleteById(id);
    }

    @Override
    public hoatDong suaHoatDong(int maHD, hoatDong hoatDong) {
        if (hoatDong != null){
            hoatDong hoatDong1 = hoatdongRes.getReferenceById(maHD);
            if (hoatDong1 != null){
                hoatDong1.setTenHD(hoatDong.getTenHD());
                hoatDong1.setMaTV(hoatDong.getMaTV());
                hoatDong1.setMoTa(hoatDong.getMoTa());
                hoatDong1.setMinQ(hoatDong.getMinQ());
                hoatDong1.setMaxQ(hoatDong.getMaxQ());
                hoatDong1.setBatDau(hoatDong.getBatDau());
                hoatDong1.setKetThuc(hoatDong.getKetThuc());
                hoatDong1.setThoiHan(hoatDong.getThoiHan());
                hoatDong1.setTrangThai(hoatDong.getTrangThai());
                hoatDong1.setLyDoHuy(hoatDong.getLyDoHuy());
                return hoatdongRes.save(hoatDong1);
            }
        }
        return null;
    }

}
