package com.example.myProject.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name ="thamgia1")
public class thamGia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "Mahd", nullable = false)
    private hoatDong maHD;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "matv")
    private thanhVien maTV;
    @Column(name = "ngay_dang_ky")
    private Date ngayDangKy;
    @Column(name = "diem_truong_doan")
    private int diemTruongDoan;
    @Column(name = "diem_tieu_chi1")
    private int diemTieuChi1;
    @Column(name = "diem_tieu_chi2")
    private int diemTieuChi2;
    @Column(name = "diem_tieu_chi3")
    private int diemTieuChi3;
    @Column(name = "nhan_xet")
    private String nhanXet;

    public thamGia() {
    }

    public thamGia(int id, hoatDong maHD, thanhVien maTV, Date ngayDangKy, int diemTruongDoan, int diemTieuChi1, int diemTieuChi2, int diemTieuChi3, String nhanXet) {
        this.id = id;
        this.maHD = maHD;
        this.maTV = maTV;
        this.ngayDangKy = ngayDangKy;
        this.diemTruongDoan = diemTruongDoan;
        this.diemTieuChi1 = diemTieuChi1;
        this.diemTieuChi2 = diemTieuChi2;
        this.diemTieuChi3 = diemTieuChi3;
        this.nhanXet = nhanXet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public hoatDong getMaHD() {
        return maHD;
    }

    public void setMaHD(hoatDong maHD) {
        this.maHD = maHD;
    }

    public thanhVien getMaTV() {
        return maTV;
    }

    public void setMaTV(thanhVien maTV) {
        this.maTV = maTV;
    }

    public Date getNgayDangKy() {
        return ngayDangKy;
    }

    public void setNgayDangKy(Date ngayDangKy) {
        this.ngayDangKy = ngayDangKy;
    }

    public int getDiemTruongDoan() {
        return diemTruongDoan;
    }

    public void setDiemTruongDoan(int diemTruongDoan) {
        this.diemTruongDoan = diemTruongDoan;
    }

    public int getDiemTieuChi1() {
        return diemTieuChi1;
    }

    public void setDiemTieuChi1(int diemTieuChi1) {
        this.diemTieuChi1 = diemTieuChi1;
    }

    public int getDiemTieuChi2() {
        return diemTieuChi2;
    }

    public void setDiemTieuChi2(int diemTieuChi2) {
        this.diemTieuChi2 = diemTieuChi2;
    }

    public int getDiemTieuChi3() {
        return diemTieuChi3;
    }

    public void setDiemTieuChi3(int diemTieuChi3) {
        this.diemTieuChi3 = diemTieuChi3;
    }

    public String getNhanXet() {
        return nhanXet;
    }

    public void setNhanXet(String nhanXet) {
        this.nhanXet = nhanXet;
    }
}
