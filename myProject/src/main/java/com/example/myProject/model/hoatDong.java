package com.example.myProject.model;



import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name ="hoatdong1")
public class hoatDong {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int maHD;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "maTV")
    private thanhVien  maTV;

    @Column(name = "tenhd")
    private String tenHD;

    @Column(name = "mo_ta")
    private String moTa;

    @Column(name = "bat_dau")
    private Date batDau;

    @Column(name = "ket_thuc")
    private Date ketThuc;

    @Column(name = "sltoi_thieu")
    private Integer minQ;

    @Column(name = "sltoi_da")
    private Integer maxQ;

    @Column(name = "thoi_han")
    private Date thoiHan;
    @Column(name = "trang_thai")
    private Boolean trangThai;
    @Column(name = "ly_do_huy")
    private String lyDoHuy;

    public hoatDong() {
    }

    public hoatDong(int maHD, thanhVien maTV, String tenHD, String moTa, Date batDau, Date ketThuc, Integer minQ, Integer maxQ, Date thoiHan, Boolean trangThai, String lyDoHuy) {
        this.maHD = maHD;
        this.maTV = maTV;
        this.tenHD = tenHD;
        this.moTa = moTa;
        this.batDau = batDau;
        this.ketThuc = ketThuc;
        this.minQ = minQ;
        this.maxQ = maxQ;
        this.thoiHan = thoiHan;
        this.trangThai = trangThai;
        this.lyDoHuy = lyDoHuy;
    }

    public int getMaHD() {
        return maHD;
    }

    public void setMaHD(int maHD) {
        this.maHD = maHD;
    }

    public thanhVien getMaTV() {
        return maTV;
    }

    public void setMaTV(thanhVien maTV) {
        this.maTV = maTV;
    }

    public String getTenHD() {
        return tenHD;
    }

    public void setTenHD(String tenHD) {
        this.tenHD = tenHD;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getBatDau() {
        return batDau;
    }

    public void setBatDau(Date batDau) {
        this.batDau = batDau;
    }

    public Date getKetThuc() {
        return ketThuc;
    }

    public void setKetThuc(Date ketThuc) {
        this.ketThuc = ketThuc;
    }

    public Integer getMinQ() {
        return minQ;
    }

    public void setMinQ(Integer minQ) {
        this.minQ = minQ;
    }

    public Integer getMaxQ() {
        return maxQ;
    }

    public void setMaxQ(Integer maxQ) {
        this.maxQ = maxQ;
    }

    public Date getThoiHan() {
        return thoiHan;
    }

    public void setThoiHan(Date thoiHan) {
        this.thoiHan = thoiHan;
    }

    public Boolean getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Boolean trangThai) {
        this.trangThai = trangThai;
    }

    public String getLyDoHuy() {
        return lyDoHuy;
    }

    public void setLyDoHuy(String lyDoHuy) {
        this.lyDoHuy = lyDoHuy;
    }
}
