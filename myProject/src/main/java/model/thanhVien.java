package model;

import javax.persistence.*;
import java.util.Date;



@Entity
@Table(name ="thanhvien")
public class thanhVien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int maTV;
    private String hoTen;
    private boolean gioiTinh;
    private Date ngaySinh;
    private String email;
    private String sdt;

    public thanhVien() {
    }

    public thanhVien(int maTV, String hoTen, boolean gioiTinh, Date ngaySinh, String email, String sdt) {
        this.maTV = maTV;
        this.hoTen = hoTen;
        this.gioiTinh = gioiTinh;
        this.ngaySinh = ngaySinh;
        this.email = email;
        this.sdt = sdt;
    }

    public int getMaTV() {
        return maTV;
    }

    public void setMaTV(int maTV) {
        this.maTV = maTV;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public boolean isGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(boolean gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }
}
