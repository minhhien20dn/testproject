package model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name ="hoatdong")
public class hoatDong {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int maHD;

    @ManyToOne
    @JoinColumn(name = "maTV")
    private thanhVien maTV;
    private String tenHD;
    private String moTa;
    private Date batDau;
    private Date ketThuc;
    private int toiThieu;
    private int toiDa;
    private Date thoiHan;
    private boolean trangThai;
    private String lyDoHuy;

    public hoatDong() {
    }

    public hoatDong(int maHD, thanhVien maTV, String tenHD, String moTa, Date batDau, Date ketThuc, int toiThieu, int toiDa, Date thoiHan, boolean trangThai, String lyDoHuy) {
        this.maHD = maHD;
        this.maTV = maTV;
        this.tenHD = tenHD;
        this.moTa = moTa;
        this.batDau = batDau;
        this.ketThuc = ketThuc;
        this.toiThieu = toiThieu;
        this.toiDa = toiDa;
        this.thoiHan = thoiHan;
        this.trangThai = trangThai;
        this.lyDoHuy = lyDoHuy;
    }

    public int getMaHD() {
        return maHD;
    }

    public void setMaHD(int maHD) {
        this.maHD = maHD;
    }

    public thanhVien getMaTV() {
        return maTV;
    }

    public void setMaTV(thanhVien maTV) {
        this.maTV = maTV;
    }

    public String getTenHD() {
        return tenHD;
    }

    public void setTenHD(String tenHD) {
        this.tenHD = tenHD;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getBatDau() {
        return batDau;
    }

    public void setBatDau(Date batDau) {
        this.batDau = batDau;
    }

    public Date getKetThuc() {
        return ketThuc;
    }

    public void setKetThuc(Date ketThuc) {
        this.ketThuc = ketThuc;
    }

    public int getToiThieu() {
        return toiThieu;
    }

    public void setToiThieu(int toiThieu) {
        this.toiThieu = toiThieu;
    }

    public int getToiDa() {
        return toiDa;
    }

    public void setToiDa(int toiDa) {
        this.toiDa = toiDa;
    }

    public Date getThoiHan() {
        return thoiHan;
    }

    public void setThoiHan(Date thoiHan) {
        this.thoiHan = thoiHan;
    }

    public boolean getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(boolean trangThai) {
        this.trangThai = trangThai;
    }

    public String getLyDoHuy() {
        return lyDoHuy;
    }

    public void setLyDoHuy(String lyDoHuy) {
        this.lyDoHuy = lyDoHuy;
    }
}
