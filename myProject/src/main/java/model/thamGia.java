package model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name ="thamgia")
public class thamGia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ma_hd_ma_hd", nullable = false)
    private hoatDong maHD;

    @ManyToOne
    @JoinColumn(name = "maTV")
    private thanhVien maTV;
    private Date ngayDangKy;
    private int diemTruongDoan;
    private int diemTieuChi1;
    private int diemTieuChi2;
    private int diemTieuChi3;
    private String nhanXet;

    public thamGia() {
    }

    public thamGia(int id, hoatDong maHD, thanhVien maTV, Date ngayDangKy, int diemTruongDoan, int diemTieuChi1, int diemTieuChi2, int diemTieuChi3, String nhanXet) {
        this.id = id;
        this.maHD = maHD;
        this.maTV = maTV;
        this.ngayDangKy = ngayDangKy;
        this.diemTruongDoan = diemTruongDoan;
        this.diemTieuChi1 = diemTieuChi1;
        this.diemTieuChi2 = diemTieuChi2;
        this.diemTieuChi3 = diemTieuChi3;
        this.nhanXet = nhanXet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public hoatDong getMaHD() {
        return maHD;
    }

    public void setMaHD(hoatDong maHD) {
        this.maHD = maHD;
    }

    public thanhVien getMaTV() {
        return maTV;
    }

    public void setMaTV(thanhVien maTV) {
        this.maTV = maTV;
    }

    public Date getNgayDangKy() {
        return ngayDangKy;
    }

    public void setNgayDangKy(Date ngayDangKy) {
        this.ngayDangKy = ngayDangKy;
    }

    public int getDiemTruongDoan() {
        return diemTruongDoan;
    }

    public void setDiemTruongDoan(int diemTruongDoan) {
        this.diemTruongDoan = diemTruongDoan;
    }

    public int getDiemTieuChi1() {
        return diemTieuChi1;
    }

    public void setDiemTieuChi1(int diemTieuChi1) {
        this.diemTieuChi1 = diemTieuChi1;
    }

    public int getDiemTieuChi2() {
        return diemTieuChi2;
    }

    public void setDiemTieuChi2(int diemTieuChi2) {
        this.diemTieuChi2 = diemTieuChi2;
    }

    public int getDiemTieuChi3() {
        return diemTieuChi3;
    }

    public void setDiemTieuChi3(int diemTieuChi3) {
        this.diemTieuChi3 = diemTieuChi3;
    }

    public String getNhanXet() {
        return nhanXet;
    }

    public void setNhanXet(String nhanXet) {
        this.nhanXet = nhanXet;
    }
}
