package service;

import model.hoatDong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import responsitory.hoatDongRes;

import java.util.List;

@Service
public class hdServiceImp implements IHoatDongService{
    @Autowired
    private hoatDongRes hoatdongRes;


    @Override
    public hoatDong themHoatDong(hoatDong hoatDong) {
        if (hoatDong != null){
            return hoatdongRes.save(hoatDong);
        }
        return null;
    }

    @Override
    public hoatDong suaHoatDong(int maHD, hoatDong hoatDong) {
        if (hoatDong != null){
            hoatDong hoatDong1 = hoatdongRes.getReferenceById(maHD);
            if (hoatDong1 != null){
                hoatDong1.setTenHD(hoatDong.getTenHD());
                hoatDong1.setMoTa(hoatDong.getMoTa());
                hoatDong1.setToiDa(hoatDong.getToiDa());
                hoatDong1.setToiThieu(hoatDong.getToiThieu());
                hoatDong1.setBatDau(hoatDong.getBatDau());
                hoatDong1.setKetThuc(hoatDong.getKetThuc());
                hoatDong1.setThoiHan(hoatDong.getThoiHan());
                hoatDong1.setTrangThai(hoatDong.getTrangThai());
                hoatDong1.setLyDoHuy(hoatDong.getLyDoHuy());
                return hoatdongRes.save(hoatDong1);
            }
        }
        return null;
    }

    @Override
    public boolean xoaHoatDong(int maHD) {
        if (maHD >= 1){
            hoatDong hoatDong1 = hoatdongRes.getReferenceById(maHD);
            if (hoatDong1 != null){
                hoatdongRes.delete(hoatDong1);
            }
        }
        return false;
    }

    @Override
    public List<hoatDong> getAll() {
        return hoatdongRes.findAll();
    }
}
