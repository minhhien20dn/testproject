package service;

import model.hoatDong;

import java.util.List;

public interface IHoatDongService {
    //them hoat dong
    public hoatDong themHoatDong(hoatDong hoatDong);

    //sua hoat dong
    public hoatDong suaHoatDong(int maHD, hoatDong hoatDong);

    //xoa hoat dong
    public boolean xoaHoatDong(int maHD);

    //view
    public List<hoatDong> getAll();

}
