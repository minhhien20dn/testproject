package controller;

import model.hoatDong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import service.IHoatDongService;

import java.util.List;

@RestController
public class hdController {
    @Autowired
    private IHoatDongService hoatDongService;

    @GetMapping("/list")
    public String index(Model model) {
        List<hoatDong> hoatDong = hoatDongService.getAll();

        model.addAttribute("hoatDong", hoatDong);

        return "index";
    }

    @PostMapping("/add")
    public hoatDong themHoatDong(@RequestBody hoatDong hoatDong){
        return hoatDongService.themHoatDong(hoatDong);
    }

    @PutMapping("/update")
    public hoatDong suaHoatDong(@RequestParam("maHD") int maHD, @RequestBody hoatDong hoatDong){
        return hoatDongService.suaHoatDong(maHD, hoatDong);
    }


}
